﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LoginRequest
    {
        public string userName { get; set; }
        public string userPass { get; set; }
    }

    public class ChangePasswordRequest
    {
        public string userName { get; set; }
        public string oldPass { get; set; }
        public string newPass { get; set; }
    }

    public class AccountModel
    {
        private TimeKeepingDB context = null;
        public AccountModel()
        {
            context = new TimeKeepingDB();
        }

        //get employee_id by account_name
        public int GetEmployeeId(string username)
        {
            string sql = "SELECT * FROM Account ";
            sql += "WHERE account_name='" + username + "'";
            Account acc = context.Database.SqlQuery<Account>(sql).SingleOrDefault();
            return acc.employee_id;
        }


        //get account
        public Account GetAccount(string username, string userpass)
        {
            string sql = "SELECT * FROM Account WHERE account_name='" + username + "' AND account_pass='" + userpass + "'";
            return context.Database.SqlQuery<Account>(sql).SingleOrDefault();
        }

        //insert account
        public int InsertAccount(Account item)
        {
            context.Account.Add(item);
            return context.SaveChanges();
        }

        //change pass
        public int ChangePassword(string username, string newPass)
        {
            string sql = "UPDATE Account SET account_pass='" + newPass + "' WHERE account_name='" + username + "'";
            return context.Database.ExecuteSqlCommand(sql);
        }

        public bool CheckLogin(string username, string userpass)
        {
            object[] sqlParam =
            {
                new SqlParameter("@userName",username),
                new SqlParameter("@userPass",userpass)
            };
            var result = context.Database.SqlQuery<bool>("usp_check_login @userName, @userPass", sqlParam).SingleOrDefault();
            return result;
        }
        public int InsertLogined(string username)
        {
            object[] sqlParam =
            {
                new SqlParameter("@time",DateTime.Now),
                new SqlParameter("@userName",username)
            };
            var result = context.Database.ExecuteSqlCommand("usp_insert_logined @time, @userName", sqlParam);
            return result;
        }
    }
}

﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class DepartmentModel
    {
        private TimeKeepingDB context = null;
        public DepartmentModel()
        {
            context = new TimeKeepingDB();
        }

        //get a department
        public Department GetDepartment(int deptId)
        {
            string sql = "SELECT * FROM Department WHERE department_id=" + deptId;
            Department result = context.Database.SqlQuery<Department>(sql).SingleOrDefault();
            return result;
        }

        //get all department
        public List<Department> GetAllDepartment()
        {
            return context.Department.ToList();
        }

        //insert a department
        public int InsertDepartment(Department item)
        {
            object[] sqlParam =
            {
                new SqlParameter("@deptName", item.department_name),
                new SqlParameter("@deptDescription", item.department_description)
            };
            var result = context.Database.ExecuteSqlCommand("usp_insert_department @deptName, @deptDescription", sqlParam);
            return result;
        }

        //update a department
        public int UpdateDepartment(Department item)
        {
            object[] sqlParam =
            {
                new SqlParameter("@deptId",item.department_id),
                new SqlParameter("@deptName", item.department_name),
                new SqlParameter("@deptDescription", item.department_description)
            };
            var result = context.Database.ExecuteSqlCommand("usp_update_department @deptId, @deptName, @deptDescription", sqlParam);
            return result;
        }

        //delete a department by department_id
        public int DeleteDepartment(int deptId)
        {
            string sql = "DELETE FROM Department WHERE department_id=" + deptId;
            var result = context.Database.ExecuteSqlCommand(sql);
            return result;
        }

        //filter
        public List<Department> FilterDepartment(string orderSort, string search)
        {
            var department = from d in context.Department select d;
            if (!String.IsNullOrEmpty(search))
            {
                department = department.Where(d => d.department_name.Contains(search));
            }
            switch (orderSort)
            {
                case "name_desc":
                    department = department.OrderByDescending(d => d.department_name);
                    break;
                default:
                    department = department.OrderBy(d => d.department_name);
                    break;
            }
            return department.ToList();
        }
    }
}

﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class EmployeeModel
    {
        private TimeKeepingDB context = null;
        public EmployeeModel()
        {
            context = new TimeKeepingDB();
        }

        //get all employee
        public List<Employee> GetAllEmployee()
        {
            return context.Employee.ToList();
        }

        //get a employee
        public Employee GetEmployee(int empId)
        {
            Employee emp = context.Employee.Where(item => item.employee_id == empId).SingleOrDefault();
            return emp;
        }

        //insert a employee
        public int InsertEmployee(Employee item)
        {
            context.Employee.Add(item);
            context.SaveChanges();
            return item.employee_id;
        }

        //update a employee
        public int UpdateEmployee(Employee item)
        {
            object[] sqlParam =
            {
                new SqlParameter("@id", item.employee_id),
                new SqlParameter("@firstName",item.employee_first_name),
                new SqlParameter("@lastName", item.employee_last_name),
                new SqlParameter("@gender",item.employee_gender),
                new SqlParameter("@birthdate", item.employee_birthdate),
                new SqlParameter("@email", item.employee_email),
                new SqlParameter("@phone",item.employee_phone),
                new SqlParameter("@address", item.employee_address),
                new SqlParameter("@status", item.employee_status),
                new SqlParameter("@deptId", item.department_id),
                new SqlParameter("@posId", item.position_id)
            };
            var result = context.Database.ExecuteSqlCommand("usp_update_employee @id, @firstName, @lastName, @gender, @birthdate, @email, @phone, @address, @status, @deptId, @posId", sqlParam);
            return result;
        }

        //filter
        public List<Employee> FilterEmployee(string sortOrder, string search)
        {
            var employee = from e in context.Employee select e;
            if (!String.IsNullOrEmpty(search))
            {
                employee = employee.Where(e => e.employee_last_name.Contains(search)
                                       || e.employee_first_name.Contains(search)
                                       || e.Department.department_name.Contains(search)
                                       || e.Position.position_name.Contains(search));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    employee = employee.OrderByDescending(e => e.employee_last_name);
                    break;
                default:
                    employee = employee.OrderBy(e => e.employee_last_name);
                    break;
            }
            return employee.ToList();
        }

    }
}

﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class HistoryModel
    {
        private TimeKeepingDB context = null;
        public HistoryModel()
        {
            context = new TimeKeepingDB();
        }

        //get history
        public List<History> GetHistory(int empId)
        {
            var list = context.History.Where(item => item.employee_id == empId).ToList();
            return list;
        }

        //insert history
        public int InsertHistory(History item)
        {

            object[] sqlParam =
            {
                new SqlParameter("@empId", item.employee_id),
                new SqlParameter("@time", DateTime.Now),
                new SqlParameter("@status", item.history_status),
                new SqlParameter("@officeId", item.office_id),
                new SqlParameter("@otherWifi", item.other_wifi)
            };
            var result = context.Database.ExecuteSqlCommand("usp_insert_history @empId, @time, @status, @officeId, @otherWifi", sqlParam);
            return result;
        }

        //filter
        public List<History> FilterHistory(int id, DateTime? start, DateTime? end)
        {
            DateTime date = DateTime.Today;
            var history = from h in context.History select h;
            if (string.IsNullOrEmpty(Convert.ToString(start)) || string.IsNullOrEmpty(Convert.ToString(end)))
            {
                history = history.Where(item => item.history_time.Value.Year == date.Year && item.history_time.Value.Month == date.Month && item.employee_id == id);
            }
            else
            {
                history = history.Where(item => item.history_time >= start && item.history_time <= end && item.employee_id == id);
            }
            return history.ToList();
        }

        //--------------
        public TimeSpan Computing(int id, DateTime? start, DateTime? end)
        {
            var history = from h in context.History select h;
            if (string.IsNullOrEmpty(Convert.ToString(start)) || string.IsNullOrEmpty(Convert.ToString(end)))
            {
                history = history.Where(item => item.employee_id == id);
            }
            else
            {
                history = history.Where(item => item.history_time >= start && item.history_time <= end && item.employee_id == id);
            }
            var list = history.ToList();
            TimeSpan ts = new TimeSpan();
            for(
                var i = list.ToArray().Length-1; i > 0; i--)
            {
                
                if (list.ToArray()[i].history_status == true)
                {
                    continue;
                }else
                {
                    for (var j = i - 1; j >= 0; j--)
                    {
                        if (list.ToArray()[j].history_status == false)
                        {
                            continue;
                        }else
                        {
                            ts += (TimeSpan)(list.ToArray()[j+1].history_time - list.ToArray()[j].history_time);
                            i--;
                            break;
                        }
                    }
                }
            }
            return ts;
        }

        public TimeSpan Computing(int id)
        {
            DateTime date = DateTime.Today;
            var list = context.History.Where(item => item.history_time.Value.Year == date.Year && item.history_time.Value.Month == date.Month && item.employee_id == id).ToList();
            TimeSpan ts = new TimeSpan();
            for (
                var i = list.ToArray().Length - 1; i > 0; i--)
            {

                if (list.ToArray()[i].history_status == true)
                {
                    continue;
                }
                else
                {
                    for (var j = i - 1; j >= 0; j--)
                    {
                        if (list.ToArray()[j].history_status == false)
                        {
                            continue;
                        }
                        else
                        {
                            ts += (TimeSpan)(list.ToArray()[j + 1].history_time - list.ToArray()[j].history_time);
                            i--;
                            break;
                        }
                    }
                }
            }
            return ts;
        }
    }
}

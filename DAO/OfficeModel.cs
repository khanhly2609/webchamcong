﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class OfficeModel
    {
        private TimeKeepingDB context = null;
        public OfficeModel()
        {
            context = new TimeKeepingDB();
        }

        //get office_id by wifi_id
        public int GetOfficeId(int wifiId)
        {
            Office off = context.Office.Where(item => item.wifi_id == wifiId).SingleOrDefault();
            return off.office_id;
        }

        //get office_id by wifi_name
        public int GetOfficeId(string wifiName)
        {
            Office off = context.Office.Where(item => item.Wifi.wifi_name == wifiName).SingleOrDefault();
            return off.office_id;
        }

        //get wifi id
        public int GetWifiId(int offId)
        {
            Office off = context.Office.Where(item => item.office_id == offId).SingleOrDefault();
            return (int)off.wifi_id;
        }

        //get office
        public Office GetOffice(int offId)
        {
            return context.Office.Where(item => item.office_id == offId).SingleOrDefault();
        }

        //get all office
        public List<Office> GetAllOffice()
        {
            return context.Office.ToList();
        }

        //insert office
        public int InsertOffice(Office off)
        {
            context.Office.Add(off);
            return context.SaveChanges();
        }

        //update office
        public int UpdateOffice(Office off)
        {
            string sql = "UPDATE Office SET ";
            sql += "office_name=N'" + off.office_name + "', wifi_id=" + off.wifi_id;
            sql += " WHERE office_id=" + off.office_id;
            return context.Database.ExecuteSqlCommand(sql);
        }

        //delete office
        public int DeleteOffice(int offId)
        {
            string sql = "DELETE FROM Office WHERE office_id=" + offId;
            return context.Database.ExecuteSqlCommand(sql);
        }

        //filter
        public List<Office> FilterOffice(string orderSort, string search)
        {
            var office = from d in context.Office select d;
            if (!String.IsNullOrEmpty(search))
            {
                office = office.Where(d => d.office_name.Contains(search));
            }
            switch (orderSort)
            {
                case "name_desc":
                    office = office.OrderByDescending(d => d.office_name);
                    break;
                default:
                    office = office.OrderBy(d => d.office_name);
                    break;
            }
            return office.ToList();
        }
    }
}

﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class PositionModel
    {
        private TimeKeepingDB context = null;
        public PositionModel()
        {
            context = new TimeKeepingDB();
        }

        //get a position
        public Position GetPosition(int positionId)
        {
            string sql = "SELECT * FROM Position WHERE position_id=" + positionId;
            var result = context.Database.SqlQuery<Position>(sql).SingleOrDefault();
            return result;
        }

        //get positions
        public List<Position> GetAllPosition()
        {
            var list = context.Database.SqlQuery<Position>("usp_list_all_position").ToList();
            return list;
        }

        //insert a Position
        public int InsertPosition(Position item)
        {
            object[] sqlParam =
            {
                new SqlParameter("@positionName", item.position_name),
                new SqlParameter("@salary", item.salary)
            };
            var result = context.Database.ExecuteSqlCommand("usp_insert_position @positionName, @salary", sqlParam);
            return result;
        }

        //update a Position
        public int UpdatePosition(Position item)
        {
            object[] sqlParam =
            {
                new SqlParameter("@positionId",item.position_id),
                new SqlParameter("@positionName", item.position_name),
                new SqlParameter("@salary", item.salary)
            };
            var result = context.Database.ExecuteSqlCommand("usp_update_position @positionId, @positionName, @salary", sqlParam);
            return result;
        }

        //delete a Position by Position_id
        public int DeletePosition(int positionId)
        {
            string sql = "DELETE FROM Position WHERE position_id=" + positionId;
            var result = context.Database.ExecuteSqlCommand(sql);
            return result;
        }

        //filter
        public List<Position> FilterPosition(string orderSort, string search)
        {
            var position = from d in context.Position select d;
            if (!String.IsNullOrEmpty(search))
            {
                position = position.Where(d => d.position_name.Contains(search));
            }
            switch (orderSort)
            {
                case "name_desc":
                    position = position.OrderByDescending(d => d.position_name);
                    break;
                default:
                    position = position.OrderBy(d => d.position_name);
                    break;
            }
            return position.ToList();
        }
    }
}

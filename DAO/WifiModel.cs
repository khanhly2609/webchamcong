﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class WifiRequest
    {
        public string wifiIp { get; set; }
        public string wifiName { get; set; }
        public string userName { get; set; }
    }
    public class WifiModel
    {
        private TimeKeepingDB context = null;
        public WifiModel()
        {
            context = new TimeKeepingDB();
        }

        //get wifi
        public Wifi GetWifi(int wifiId)
        {
            var wifi = context.Wifi.Where(item => item.wifi_id == wifiId).SingleOrDefault();
            return wifi;
        }
        public Wifi GetWifi(string wifiName)
        {
            return context.Wifi.Where(item => item.wifi_name == wifiName).SingleOrDefault();
        }

        //get all wifi
        public List<Wifi> GetAllWifi()
        {
            return context.Wifi.ToList();
        }

        //insert wifi
        public int InsertWifi(Wifi wifi)
        {
            context.Wifi.Add(wifi);
            context.SaveChanges();
            return wifi.wifi_id;
        }

        //update wifi
        public int UpdateWifi(Wifi wifi)
        {
            string sql = "UPDATE Wifi SET wifi_name=N'" + wifi.wifi_name + "' ";
            sql += "WHERE wifi_id=" + wifi.wifi_id;
            return context.Database.ExecuteSqlCommand(sql);
        }

        //delete wifi
        public int DeleteWifi(int wfId)
        {
            string sql = "DELETE FROM Wifi WHERE wifi_id=" + wfId;
            return context.Database.ExecuteSqlCommand(sql);
        }

        //check wifi existing
        public bool isExists(Wifi wifi)
        {
            bool flag = false;
            object[] sqlParam =
            {
                new SqlParameter("@wifiIp", wifi.wifi_ip),
                new SqlParameter("@wifiName",wifi.wifi_name)
            };
            if (context.Database.SqlQuery<bool>("usp_check_wifi @wifiIp, @wifiName", sqlParam).SingleOrDefault())
            {
                flag = true;
            }
            return flag;
        }

        //check status by wifi_name
        public bool CheckWifi(string wifiName)
        {
            var result = context.Database.SqlQuery<bool>("usp_check_wifi_by_wifi_name @wifiName", new SqlParameter("@wifiName", wifiName)).SingleOrDefault();
            return result;
        }

        //check status 
        public bool CheckWifi(string wifiName, string wifiIp)
        {
            object[] sqlParam =
            {
                new SqlParameter("@wifiIp", wifiIp),
                new SqlParameter("@wifiName",wifiName)
            };
            return context.Database.SqlQuery<bool>("usp_check_wifi @wifiIp, @wifiName", sqlParam).SingleOrDefault();
        }
    }
}

namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            Account = new HashSet<Account>();
            History = new HashSet<History>();
        }

        [Key]
        public int employee_id { get; set; }

        [StringLength(30)]
        public string employee_first_name { get; set; }

        [StringLength(10)]
        public string employee_last_name { get; set; }

        public bool? employee_gender { get; set; }

        [Column(TypeName = "date")]
        public DateTime? employee_birthdate { get; set; }

        [Required]
        [StringLength(50)]
        public string employee_email { get; set; }

        [StringLength(10)]
        public string employee_phone { get; set; }

        [StringLength(200)]
        public string employee_address { get; set; }

        public bool? employee_status { get; set; }

        public int department_id { get; set; }

        public int position_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Account { get; set; }

        public virtual Department Department { get; set; }

        public virtual Position Position { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<History> History { get; set; }
    }
}

namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Globalization;

    [Table("History")]
    public partial class History
    {
        [Key]
        public int history_id { get; set; }

        public DateTime? history_time { get; set; }

        public bool? history_status { get; set; }

        public int? employee_id { get; set; }

        public int? office_id { get; set; }

        [StringLength(50)]
        public string other_wifi { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Office Office { get; set; }
    }
}

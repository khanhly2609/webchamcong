namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Logined")]
    public partial class Logined
    {
        [Key]
        public int logined_id { get; set; }

        public DateTime? logined_time { get; set; }

        [StringLength(50)]
        public string account_name { get; set; }

        public virtual Account Account { get; set; }
    }
}

namespace Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TimeKeepingDB : DbContext
    {
        public TimeKeepingDB()
            : base("name=TimeKeepingDB")
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<History> History { get; set; }
        public virtual DbSet<Logined> Logined { get; set; }
        public virtual DbSet<Office> Office { get; set; }
        public virtual DbSet<Position> Position { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Wifi> Wifi { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(e => e.account_name)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.account_pass)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.employee_email)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.employee_phone)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Account)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Logined>()
                .Property(e => e.account_name)
                .IsUnicode(false);

            modelBuilder.Entity<Position>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.Position)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Wifi>()
                .Property(e => e.wifi_ip)
                .IsUnicode(false);
        }
    }
}

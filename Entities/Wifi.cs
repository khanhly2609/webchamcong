namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Wifi")]
    public partial class Wifi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Wifi()
        {
            Office = new HashSet<Office>();
        }

        [Required]
        [StringLength(50)]
        public string wifi_name { get; set; }

        [StringLength(50)]
        public string wifi_ip { get; set; }

        [Key]
        public int wifi_id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Office> Office { get; set; }
    }
}

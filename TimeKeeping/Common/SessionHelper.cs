﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeKeeping.Common
{
    public class SessionHelper
    {
        public static void setSession(UserSession ss)
        {
            HttpContext.Current.Session["loginSession"] = ss;
        }

        public static UserSession getSession()
        {
            var session = HttpContext.Current.Session["loginSession"];
            if (session == null)
            {
                return null;
            }
            else
            {
                return session as UserSession;
            }
        }
    }
}
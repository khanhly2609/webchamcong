﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace TimeKeeping.Common
{
    public class StringUtils
    {
        public void SendEmail(string emailTo, string subject, string content)
        {
            var fromEmailAddress = ConfigurationManager.AppSettings["EmailFrom"].ToString();
            var displayName = ConfigurationManager.AppSettings["DisplayName"].ToString();
            var emailPass = ConfigurationManager.AppSettings["EmailPass"].ToString();
            var smtpHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            var smtpPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();

            bool enableSsl = bool.Parse(ConfigurationManager.AppSettings["EnableSSL"].ToString());

            string body = content;

            MailMessage msg = new MailMessage(new MailAddress(fromEmailAddress, displayName), new MailAddress(emailTo));
            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = body;

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAddress, emailPass);
            client.Host = smtpHost;
            client.EnableSsl = enableSsl;
            client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
            client.Send(msg);
        }

        public string CreateUserName(string email)
        {
            Char charRange = '@';
            int startIndex = 0;
            int endIndex = email.LastIndexOf(charRange);
            int length = endIndex - startIndex;
            return email.Substring(startIndex, length);
        }

        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        public static string MH(string str)
        {
            MD5 mh = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(str);
            byte[] hash = mh.ComputeHash(inputBytes);
            StringBuilder md = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                md.Append(hash[i].ToString("X2"));
            }
            return md.ToString();
        }

        public TimeSpan TimeComputing(DateTime? start, DateTime? end)
        {
            TimeSpan ts = new TimeSpan();

            return ts;
        }
    }
}
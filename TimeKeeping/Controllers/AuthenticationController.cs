﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    [RoutePrefix("api/authen")]
    public class AuthenticationController : ApiController
    {
        [Route("login")]
        [HttpPost]
        public object Login(LoginRequest request)
        {

            string loginResponse = "";
            var implLogin = new AccountModel();
            var validAccount = implLogin.CheckLogin(request.userName, request.userPass);
            if (!validAccount)
            {
                loginResponse += "Sai tên đăng nhập hoặc mật khẩu!";
            }
            else
            {
                loginResponse += "Đăng nhập thành công!";
                var insertLogined = implLogin.InsertLogined(request.userName);
                if (insertLogined != 1)
                {
                    loginResponse += " Lỗi: Không thể ghi lại đăng nhập!";
                }
            }
            return new
            {
                login_message = loginResponse
            };
        }

        [Route("change-pass")]
        [HttpPost]
        public object ChangePassword(ChangePasswordRequest request)
        {
            string changeResponse = "";

            var implAccount = new AccountModel();

            if (implAccount.GetAccount(request.userName, StringUtils.MH(request.oldPass)) != null)
            {
                try
                {
                    implAccount.ChangePassword(request.userName, StringUtils.MH(request.newPass));
                    changeResponse = "Đổi mật khẩu thành công!";
                }
                catch
                {
                    changeResponse = "Đổi mật khẩu thất bại!";
                }
            }
            else
            {
                changeResponse = "Sai mật khẩu!";
            }

            return new
            {
                changePass_message = changeResponse
            };
        }

        [Route("reset-pass")]
        [HttpPost]
        public object ResetPassword(string username)
        {
            string resetPassResponse = "";
            var implAccount = new AccountModel();
            var newPass = new StringUtils().CreatePassword(8);
            var email = new EmployeeModel().GetEmployee(implAccount.GetEmployeeId(username)).employee_email;
            try
            {
                implAccount.ChangePassword(username, StringUtils.MH(newPass));
                string content = "New pass: " + newPass;
                new StringUtils().SendEmail(email, "Đặt lại mật khẩu", content);
                resetPassResponse += "Đặt lại mật khẩu thành công. Mật khẩu mới sẽ được gửi vào địa chỉ email của bạn!";
            }
            catch
            {
                resetPassResponse += "Lỗi: Không thể đặt lại mật khẩu!";
            }
            return new
            {
                resetPass_message = resetPassResponse
            };
        }
    }
}

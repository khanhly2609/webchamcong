﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var sess = SessionHelper.getSession();
            if (sess == null)
            {
                filterContext.Result = new RedirectToRouteResult(
           new RouteValueDictionary{{ "controller", "Login" },
                                          { "action", "Index" }

                                         });
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
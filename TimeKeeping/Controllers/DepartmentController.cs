﻿using DAO;
using Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    public class DepartmentController : BaseController
    {
        // GET: Department
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;
            ViewBag.Posiion = emp.Position.position_name;
            ViewBag.Department = emp.Department.department_name;

            ViewBag.NameSortParam = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var department = new DepartmentModel().FilterDepartment(sortOrder, searchString);

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(department.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, Department item)
        {
            item.department_name = fc["txtDepartmentName"];
            item.department_description = fc["txtDepartmentDescription"];
            var implDepartment = new DepartmentModel();
            try
            {
                implDepartment.InsertDepartment(item);
                TempData["msg"] = "<script>alert('Thêm mới thành công!');</script>";
            }
            catch
            {
                TempData["msg"] = "<script>alert('Thêm mới không thành công!');</script>";
            }
            return RedirectToAction("Index");
        }

        public ViewResult Update(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;
            ViewBag.Posiion = emp.Position.position_name;
            ViewBag.Department = emp.Department.department_name;

            var id = Int32.Parse(HttpContext.Request.QueryString["id"]);
            var department = new DepartmentModel().GetDepartment(id);
            ViewData["deptName"] = department.department_name;
            ViewData["deptDescription"] = department.department_description;
            ViewBag.NameSortParam = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var dept = new DepartmentModel().FilterDepartment(sortOrder, searchString);

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(dept.ToPagedList(pageNumber, pageSize));

        }

        [HttpPost]
        public ActionResult Update(FormCollection fc, Department item)
        {
            item.department_id = Int32.Parse(HttpContext.Request.QueryString["id"]);
            item.department_name = fc["txtDepartmentName"];
            item.department_description = fc["txtDepartmentDescription"];
            var implDepartment = new DepartmentModel();
            try
            {
                implDepartment.UpdateDepartment(item);
                TempData["msg"] = "<script>alert('Cập nhật thành công!');</script>";
            }
            catch
            {
                TempData["msg"] = "<script>alert('Cập nhật không thành công!');</script>";
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var implDepartment = new DepartmentModel();
            try
            {
                implDepartment.DeleteDepartment(id);
                TempData["msg"] = "<script>alert('Xóa thành công!');</script>";
            }
            catch
            {
                TempData["msg"] = "<script>alert('Xóa thất bại!');</script>";
            }
            return RedirectToAction("Index");
        }
    }
}
﻿using DAO;
using Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    public class EmployeeController : BaseController
    {

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            ViewBag.NameSortParam = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var employees = new EmployeeModel().FilterEmployee(sortOrder, searchString);

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(employees.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Create()
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            var implDepartment = new DepartmentModel();
            var implPosition = new PositionModel();
            ViewBag.Department = new SelectList(implDepartment.GetAllDepartment(), "department_id", "department_name");
            ViewBag.Position = new SelectList(implPosition.GetAllPosition(), "position_id", "position_name");
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection fc, Employee item)
        {
            item.employee_first_name = fc["txtFirstName"];
            item.employee_last_name = fc["txtLastName"];
            item.employee_gender = true;
            if (fc["rbtnGender"].ToString() == "female")
            {
                item.employee_gender = false;
            }
            item.employee_birthdate = Convert.ToDateTime(fc["txtBirthdate"]);
            item.employee_email = fc["txtEmail"];
            item.employee_phone = fc["txtPhone"];
            item.employee_address = fc["txtAddress"];
            item.employee_status = true;
            item.department_id = Int32.Parse(fc["ddlDepartment"]);
            item.position_id = Int32.Parse(fc["ddlPosition"]);
            var implEmployee = new EmployeeModel();
            try
            {
                var empId = implEmployee.InsertEmployee(item);
                var acc = new Account();
                var emp = implEmployee.GetEmployee(empId);
                var pass = new StringUtils().CreatePassword(8);
                acc.employee_id = empId;
                acc.account_name = new StringUtils().CreateUserName(emp.employee_email);
                acc.account_pass = StringUtils.MH(pass);
                acc.permision = 1;
                try
                {
                    new AccountModel().InsertAccount(acc);

                    string mess = "Account: " + acc.account_name + " <br/>Password: " + pass;

                    new StringUtils().SendEmail(emp.employee_email, "Tạo tài khoản thành công", mess);
                }
                catch
                {
                    TempData["msg"] = "<script>alert('Lỗi: không thể tạo tài khoản!');</script>";
                }
                TempData["msg"] = "<script>alert('Thêm mới thành công!');</script>";
            }
            catch
            {
                TempData["msg"] = "<script>alert('Thêm mới không thành công!');</script>";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Update()
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            var id = Int32.Parse(HttpContext.Request.QueryString["id"]);
            var item = new EmployeeModel().GetEmployee(id);
            ViewBag.Gender = item.employee_gender;
            ViewBag.Status = item.employee_status;
            ViewData["firstName"] = item.employee_first_name;
            ViewData["lastName"] = item.employee_last_name;
            ViewData["birthdate"] = Convert.ToDateTime(item.employee_birthdate).ToShortDateString();
            ViewData["email"] = item.employee_email;
            ViewData["phone"] = item.employee_phone;
            ViewData["address"] = item.employee_address;
            ViewBag.Department = new SelectList(new DepartmentModel().GetAllDepartment(), "department_id", "department_name", item.department_id);
            ViewBag.Position = new SelectList(new PositionModel().GetAllPosition(), "position_id", "position_name", item.position_id);
            return View();
        }

        [HttpPost]
        public ActionResult Update(FormCollection fc, Employee item)
        {
            item.employee_id = Int32.Parse(HttpContext.Request.QueryString["id"]);
            item.employee_first_name = fc["txtFirstName"];
            item.employee_last_name = fc["txtLastName"];
            item.employee_gender = true;
            if (fc["rbtnGender"].ToString() == "female")
            {
                item.employee_gender = false;
            }
            try
            {
                item.employee_birthdate = Convert.ToDateTime(fc["txtBirthdate"]);
            }
            catch
            {
                item.employee_birthdate = new EmployeeModel().GetEmployee(item.employee_id).employee_birthdate;
            }
            item.employee_email = fc["txtEmail"];
            item.employee_phone = fc["txtPhone"];
            item.employee_address = fc["txtAddress"];
            item.department_id = Int32.Parse(fc["ddlDepartment"]);
            item.position_id = Int32.Parse(fc["ddlPosition"]);
            item.employee_status = true;
            if (fc["rbtnStatus"].ToString() == "inactive")
            {
                item.employee_status = false;
            }
            var implEmployee = new EmployeeModel();
            try
            {
                implEmployee.UpdateEmployee(item);
                TempData["msg"] = "<script>alert('Cập nhật thành công!');</script>";
            }
            catch
            {
                TempData["msg"] = "<script>alert('Cập nhật không thành công!');</script>";
            }
            return RedirectToAction("Index");
        }

        public ActionResult History(DateTime? cstart, DateTime? cend, DateTime? start, DateTime? end, int? page)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);

            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            var id = Int32.Parse(HttpContext.Request.QueryString["employeeId"]);
            var employee = new EmployeeModel().GetEmployee(id);
            ViewData["_fullName"] = employee.employee_first_name + " " + employee.employee_last_name;
            ViewData["gender"] = employee.employee_gender == true ? "Nam" : "Nữ";
            ViewData["birthdate"] = Convert.ToDateTime(employee.employee_birthdate).ToShortDateString();
            ViewData["email"] = employee.employee_email;
            ViewData["phone"] = employee.employee_phone;
            ViewData["address"] = employee.employee_address;
            ViewData["department"] = employee.Department.department_name;
            ViewData["position"] = employee.Position.position_name;

            if (start != null && end != null)
            {
                page = 1;
            }
            else
            {
                start = cstart;
                end = cend;
            }

            ViewBag.Start = start;
            ViewBag.End = end;
            ViewBag.id = id;

            TimeSpan ts = new HistoryModel().Computing(id);

            ViewBag.Time = string.Format("{0:00}:{1:00}:{2:00}", ts.TotalHours, ts.Minutes, ts.Seconds);

            var model = new HistoryModel().FilterHistory(id, start, end);

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(model.ToPagedList(pageNumber, pageSize));
        }

        
    }
}
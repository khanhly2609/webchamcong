﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;
            return View();
        }

    }
}
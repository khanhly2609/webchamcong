﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignOut()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginRequest request)
        {
            var result = new AccountModel().CheckLogin(request.userName, request.userPass);
            if (result)
            {
                var acc = new AccountModel().GetAccount(request.userName, request.userPass);
                if (acc.permision != 0)
                {
                    result = false;
                }
            }
            if (result && ModelState.IsValid)
            {
                SessionHelper.setSession(new UserSession() { userName = request.userName });
                try
                {
                    new AccountModel().InsertLogined(request.userName);
                }
                catch
                {

                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Đăng nhập thất bại!");
            }
            return View(request);
        }
    }
}
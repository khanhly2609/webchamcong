﻿using DAO;
using Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    public class OfficeController : BaseController
    {
        // GET: Office
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            ViewBag.NameSortParam = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var office = new OfficeModel().FilterOffice(sortOrder, searchString);

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(office.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, Office off, Wifi wf)
        {
            wf.wifi_name = fc["txtWifiName"];
            wf.wifi_ip = fc["txtWifiIp"];
            try
            {
                var id = new WifiModel().InsertWifi(wf);
                if (id != 0)
                {
                    off.office_name = fc["txtOfficeName"];
                    off.wifi_id = id;
                    off.office_id = 1;
                    var implOffice = new OfficeModel();
                    try
                    {
                        implOffice.InsertOffice(off);
                        TempData["msg"] = "<script>alert('Thêm mới thành công!');</script>";
                    }
                    catch
                    {
                        TempData["msg"] = "<script>alert('Thêm mới không thành công!');</script>";
                    }
                }
                else
                {
                    TempData["msg"] = "<script>alert('Thêm mới không thành công!');</script>";
                }
            }
            catch
            {
                TempData["msg"] = "<script>alert('Thêm mới không thành công! Wifi đã tồn tại!');</script>";
            }

            return RedirectToAction("Index");
        }

        public ViewResult Update(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            //var model = new OfficeModel().GetAllOffice();
            var id = Int32.Parse(HttpContext.Request.QueryString["id"]);
            var office = new OfficeModel().GetOffice(id);
            ViewData["officeName"] = office.office_name;
            ViewData["wifiName"] = office.Wifi.wifi_name;
            ViewData["wifiIp"] = office.Wifi.wifi_ip;

            ViewBag.NameSortParam = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var off = new OfficeModel().FilterOffice(sortOrder, searchString);

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(off.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult Update(FormCollection fc, Office item)
        {
            var implOffice = new OfficeModel();
            var implWifi = new WifiModel();
            item.office_id = Int32.Parse(HttpContext.Request.QueryString["id"]);
            item.office_name = fc["txtOfficeName"];
            item.wifi_id = implOffice.GetWifiId(Int32.Parse(HttpContext.Request.QueryString["id"]));
            var inputWifi = new Wifi()
            {
                wifi_id = (int)item.wifi_id,
                wifi_name = fc["txtwifiName"],
                wifi_ip = fc["txtWifiIp"]
            };
            if (!implWifi.isExists(inputWifi))
            {
                try
                {
                    item.wifi_id = implWifi.InsertWifi(inputWifi);
                }
                catch
                {
                    implWifi.UpdateWifi(inputWifi);
                }
            }
            implOffice.UpdateOffice(item);
            TempData["msg"] = "<script>alert('Cập nhật thành công!');</script>";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var implOffice = new OfficeModel();
            var off = implOffice.GetOffice(id);
            try
            {
                implOffice.DeleteOffice(id);
                new WifiModel().DeleteWifi((int)off.wifi_id);
                TempData["msg"] = "<script>alert('Xóa thành công!');</script>";
            }
            catch
            {
                TempData["msg"] = "<script>alert('Xóa thất bại!');</script>";
            }
            return RedirectToAction("Index");
        }
    }
}
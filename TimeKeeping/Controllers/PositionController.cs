﻿using DAO;
using Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    public class PositionController : BaseController
    {
        // GET: Position
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            ViewBag.NameSortParam = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var position = new PositionModel().FilterPosition(sortOrder, searchString);

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(position.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, Position item)
        {
            item.position_name = fc["txtPositionName"];
            item.salary = Int32.Parse(fc["txtSalary"]);
            var implPosition = new PositionModel();
            try
            {
                implPosition.InsertPosition(item);
                TempData["msg"] = "<script>alert('Thêm mới thành công!');</script>";
            }
            catch
            {
                TempData["msg"] = "<script>alert('Thêm mới không thành công!');</script>";
            }
            return RedirectToAction("Index");
        }

        public ViewResult Update(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            //var model = new PositionModel().GetAllPosition();
            var id = Int32.Parse(HttpContext.Request.QueryString["id"]);
            var pos = new PositionModel().GetPosition(id);
            ViewData["positionName"] = pos.position_name;
            ViewData["salary"] = pos.salary;

            ViewBag.NameSortParam = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var item = new PositionModel().FilterPosition(sortOrder, searchString);

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(item.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult Update(FormCollection fc, Position item)
        {
            item.position_id = Int32.Parse(HttpContext.Request.QueryString["id"]);
            item.position_name = fc["txtPositionName"];
            item.salary = Int32.Parse(fc["txtSalary"]);
            var implPosition = new PositionModel();
            if (implPosition.UpdatePosition(item) != 1)
            {
                TempData["msg"] = "<script>alert('Cập nhật không thành công!');</script>";
            }
            TempData["msg"] = "<script>alert('Cập nhật thành công!');</script>";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var implPosition = new PositionModel();
            if (implPosition.DeletePosition(id) != 1)
            {
                TempData["msg"] = "<script>alert('Xóa thất bại!');</script>";
            }
            TempData["msg"] = "<script>alert('Xóa thành công!');</script>";
            return RedirectToAction("Index");
        }
    }
}
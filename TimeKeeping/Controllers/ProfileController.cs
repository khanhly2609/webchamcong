﻿using DAO;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeKeeping.Common;

namespace TimeKeeping.Controllers
{
    public class ProfileController : BaseController
    {
        // GET: Profile
        public ActionResult Index()
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            ViewData["Gender"] = emp.employee_gender == true ? "Nam" : "Nữ";
            ViewData["Birhdate"] = Convert.ToDateTime(emp.employee_birthdate).ToShortDateString();
            ViewData["Phone"] = emp.employee_phone;
            ViewData["Email"] = emp.employee_email;
            ViewData["Address"] = emp.employee_address;

            return View();
        }

        public ActionResult Update()
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;
            ViewData["firstName"] = emp.employee_first_name;
            ViewData["lastName"] = emp.employee_last_name;
            ViewData["birthdate"] = emp.employee_birthdate;
            ViewBag.Gender = emp.employee_gender;
            ViewData["email"] = emp.employee_email;
            ViewData["phone"] = emp.employee_phone;
            ViewData["address"] = emp.employee_address;
            return View();
        }

        [HttpPost]
        public ActionResult Update(FormCollection fc, Employee item)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            item.employee_id = empId;
            item.employee_first_name = fc["txtFirstName"];
            item.employee_last_name = fc["txtLastName"];
            try
            {
                item.employee_birthdate = Convert.ToDateTime(fc["txtBirthdate"]);
            }
            catch
            {
                item.employee_birthdate = emp.employee_birthdate;
            }
            if (fc["rbtnGender"].ToString() == "female")
            {
                item.employee_gender = false;
            }
            item.employee_email = fc["txtEmail"];
            item.employee_phone = fc["txtPhone"];
            item.employee_address = fc["txtAddress"];
            item.department_id = emp.department_id;
            item.position_id = emp.position_id;
            item.employee_status = emp.employee_status;
            var implEmployee = new EmployeeModel();
            try
            {
                implEmployee.UpdateEmployee(item);
                TempData["msg"] = "<script>alert('Cập nhật thành công!');</script>";
            }
            catch
            {
                TempData["msg"] = "<script>alert('Cập nhật không thành công!');</script>";
            }

            return RedirectToAction("Index");
        }

        public ActionResult ChangePass()
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;
            return View();
        }

        [HttpPost]
        public ActionResult ChangePass(FormCollection fc)
        {
            var account_name = SessionHelper.getSession().userName;
            var empId = new AccountModel().GetEmployeeId(account_name);
            var emp = new EmployeeModel().GetEmployee(empId);
            ViewBag.FullName = emp.employee_first_name + " " + emp.employee_last_name;
            ViewBag.UserName = account_name;

            var implAccount = new AccountModel();

            var oldPass = fc["txtOldPass"];
            var newPass = fc["txtNewPass"];
            var confirm = fc["txtConfirmPass"];

            if (implAccount.GetAccount(account_name, oldPass) != null)
            {
                if (newPass == confirm)
                {
                    try
                    {
                        implAccount.ChangePassword(account_name, newPass);
                        TempData["msg"] = "<script>alert('Cập nhật mật khẩu thành công!');</script>";
                    }
                    catch
                    {
                        TempData["msg"] = "<script>alert('Cập nhật mật khẩu thất bại!');</script>";
                    }
                }
                else
                {
                    TempData["msg"] = "<script>alert('Xác nhận mật khẩu mới không khớp!');</script>";
                }
            }
            else
            {
                TempData["msg"] = "<script>alert('Sai mật khẩu!');</script>";
            }

            return RedirectToAction("Index");
        }
    }
}
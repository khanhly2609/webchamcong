﻿using DAO;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TimeKeeping.Controllers
{
    public class WifiConnectionController : ApiController
    {
        [HttpPost]
        public object WifiConnection(WifiRequest request)
        {
            //WifiRequest request = new WifiRequest
            //{
            //    userName="kminerin003",
            //    wifiIp= "192.168.0.145",
            //    wifiName= "Redmi4"
            //};
            string wifiResponse = "";
            var implWifiConnection = new WifiModel();
            var isOfficeWifi = implWifiConnection.CheckWifi(request.wifiName, request.wifiIp);
            var history = new History()
            {
                history_status = true,
                employee_id = new AccountModel().GetEmployeeId(request.userName),
                other_wifi = request.wifiName
            };
            if (!isOfficeWifi)
            {
                wifiResponse += "Out of Office!";
                history.history_status = false;
                history.office_id = 0;
            }
            else
            {
                wifiResponse += "In Office!";
                history.office_id = new OfficeModel().GetOfficeId(request.wifiName);
            }
            try
            {
                new HistoryModel().InsertHistory(history);
            }
            catch
            {
                wifiResponse += " Err: Insert history failure!";
            }
            return new
            {
                wifiConnection_message = wifiResponse
            };
        }
    }
}
